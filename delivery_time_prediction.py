import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
os.chdir("C://Users//HP//Desktop//data science//slr")
ass=pd.read_csv("delivery_time.csv")
ass.head(9)
ass.rename({"Delivery Time" : "delivery","Sorting Time" : "sorting"},axis=1,inplace=True)
plt.boxplot(ass.delivery,0,"rs",0)
plt.hist(ass.delivery)
plt.boxplot(ass.sorting,0,"rs",0)
plt.hist(ass.sorting)
ass.delivery.corr(ass.sorting)
plt.plot(ass.delivery,ass.sorting);plt.xlabel("x");plt.ylabel("y")
np.corrcoef(ass.sorting,ass.delivery)

import statsmodels.formula.api as api
model = api.ols('sorting~delivery',data=ass).fit()
model.params
model.summary()
pred=model.predict(ass.delivery)
ass.columns
pred.corr(ass.sorting)
plt.scatter(x=ass.delivery,y=ass.sorting,c="r");plt.plot(ass.delivery,pred,"b");plt.xlabel("delivery");plt.ylabel("sorting")
import statsmodels.formula.api as api
model1 = api.ols('sorting~np.log(delivery)',data=ass).fit()
model1.params
model1.summary()
pred1=model1.predict(ass.delivery)
ass.columns
pred1.corr(ass.sorting)
plt.scatter(x=ass.delivery,y=ass.sorting,c="r");plt.plot(ass.delivery,pred1,"b");plt.xlabel("delivery");plt.ylabel("sorting")
import statsmodels.formula.api as api
model2 = api.ols('np.log(sorting)~delivery',data=ass).fit()
model2.params
model2.summary()
pred_log=model2.predict(ass.delivery)
pred2=np.exp(pred_log)
pred2.corr(ass.sorting)
plt.scatter(x=ass.delivery,y=ass.sorting,c="r");plt.plot(ass.delivery,pred,"b");plt.xlabel("delivery");plt.ylabel("sorting")

ass["model_quad"]= ass.delivery*ass.delivery
model3=api.ols("sorting~delivery+model_quad",data=ass).fit()
model3.params
model3.summary()
pred3=model3.predict(ass[['delivery','model_quad']])
ass.columns
pred3.corr(ass.sorting)
plt.scatter(x=ass.delivery,y=ass.sorting,c="r");plt.plot(ass.delivery,pred3,"b");plt.xlabel("delivery");plt.ylabel("sorting")



from sklearn.model_selection import train_test_split 
train,test =train_test_split(ass,test_size=0.05)
train1=api.ols("sorting~delivery+model_quad",data=train).fit()
train1.params
train1.summary()
pred6=train1.predict(train[['delivery','model_quad']])
pred7=train1.predict(test[['delivery','model_quad']])

np.corrcoef(train.sorting,pred6)
np.corrcoef(test.sorting,pred7)

pr=pd.DataFrame({'delivery':[30,10,20],'model_quad':[900,100,400]})
pred8=train1.predict(pr) 
pred8
